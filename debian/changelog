z88 (15+dfsg-1) unstable; urgency=medium

  * Team upload
  * Adhere to machine-readable copyright format
  * New upstream version 15+dfsg (Closes: #1053735)
  * Rebase patches
  * Build package with original build system (Closes: #1077953)

 -- Bastian Germann <bage@debian.org>  Thu, 05 Dec 2024 21:57:47 +0000

z88 (13.0.0+dfsg2-7) unstable; urgency=medium

  * Team Upload
  * Fix watch file for new GitHub API
  * Replace old pkg-config with pkgconf
  * Switch to DebHelper 13
  * Set Rules-Requires-Root: no

  [ Debian Janitor ]
  * Apply multi-arch hints. + z88-data, z88-doc: Add Multi-Arch: foreign.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 03 Jul 2024 14:21:44 +0200

z88 (13.0.0+dfsg2-6) unstable; urgency=medium

  [ Gert Wollny ]
  * d/p/70: Update patch to use pkg-config for gtkglext

  [ Matteo F. Vescovi ]
  * debian/control: S-V bump 4.1.3 -> 4.1.4 (no changes needed)

 -- Matteo F. Vescovi <mfv@debian.org>  Fri, 13 Apr 2018 14:09:35 +0200

z88 (13.0.0+dfsg2-5) unstable; urgency=medium

  [ Andreas Tille ]
  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * Despite there is no active Uploader from Debian Science the package
    should be in Debian Science maintenance
  * debhelper 11
  * Fix homepage
  * d/watch: point to Github

  [ Matteo F. Vescovi ]
  * debian/control: add myself to Uploaders (Closes: #622191)
  * debian/menu: removed (obsolete)
  * debian/copyright: fix license header and description
  * debian/z88-doc.doc-base: fix path for documentation

 -- Matteo F. Vescovi <mfv@debian.org>  Sat, 17 Feb 2018 15:15:01 +0100

z88 (13.0.0+dfsg2-4) unstable; urgency=medium

  * QA upload.
  * Switch to dh.
  * Build verbosely.
  * Raise debhelper compat level to 9.
  * Drop get-orig-source target, since it does not seem to work.

 -- Santiago Vila <sanvila@debian.org>  Thu, 04 Aug 2016 11:30:08 +0100

z88 (13.0.0+dfsg2-3) unstable; urgency=low

  * QA upload.
  * Add multiarch support. Thanks to Micah Gersten! (Closes: #639059)
    - debian/control: Add pkg-config as a build-dependency.
    - debian/patches/70_use_pkg_config.patch: Use pkg-config
      to find GTK and GLIB include paths.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Tue, 13 Sep 2011 10:16:21 +0200

z88 (13.0.0+dfsg2-2) unstable; urgency=low

  * QA upload.
  * Change maintainer to Debian QA Group.
  * Switch to format 3.0 (quilt).
    - Add debian/source/format and debian/source/local-options.
    - Adapt debian/patches to quilt.
    - Adapt debian/rules.
    - Remove dpatch as a dependency.
  * src/CMakeLists.txt: Include /usr/include/gdk-pixbuf-2.0/
    to build successfully (Closes: #621950, #622194)
  * 60-fix-spelling-errors.patch: Fix some spelling errors.
  * Remove Section field in z88-data to avoid override disparities.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Mon, 04 Jul 2011 08:58:24 +0200

z88 (13.0.0+dfsg2-1) unstable; urgency=low

  * New upstream version doesn't depend on libmotif-dev anymore. It's
    using GTK+ instead. Because z88 doesn't depend on libmotif-dev
    anymore the package can go from contrib to main.
  * z88-data will be build only in build-indep not in build-arch anymore
  * debian/get-orig-source: upstream's source package doesn't separate
    source files, data files, documentation files and build files.
    debian/get-orig-source creates some subfolder for a better overview

 -- Dominique Belhachemi <domibel@cs.tu-berlin.de>  Sat, 04 Apr 2009 12:49:38 -0400

z88 (13.0.0+dfsg1-1) unstable; urgency=low

  * New upstream version

 -- Dominique Belhachemi <domibel@cs.tu-berlin.de>  Thu, 02 Apr 2009 23:25:34 -0400

z88 (12.0.1+dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #516333)

 -- Dominique Belhachemi <domibel@cs.tu-berlin.de>  Sun, 15 Feb 2009 03:21:31 -0500
